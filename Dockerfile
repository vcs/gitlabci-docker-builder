FROM docker:1.10

MAINTAINER alexandre.lossent@cern.ch

COPY build.sh /

RUN chmod 755 build.sh

ENTRYPOINT /build.sh
