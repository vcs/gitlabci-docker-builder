#!/bin/sh

# Builds a Docker container and optionally uploads it to a registry.
# This is meant to use as the entrypoint of a Docker image used by a gitlab-ci job.
# Because we ignore the CMD passed by Docker, users cannot run their own CI script, making it
# safe to give privileges to containers executing this image (such as --privileged for Docker-in-Docker
# or bind-mounting /var/run/docker.sock to use the host's Docker daemon)
# See also https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/executors/docker.md#the-entrypoint

# folder to build from (default current directory)
CONTEXT_DIR="${CONTEXT_DIR:-.}"

# a docker registry to docker login into (optional)
DOCKER_LOGIN_SERVER="${DOCKER_LOGIN_SERVER:-${CI_REGISTRY}}"
# username to use with docker login (when DOCKER_LOGIN_SERVER is specified)
DOCKER_LOGIN_USERNAME="${DOCKER_LOGIN_USERNAME:-gitlab-ci-token}"
# password to use with docker login (when DOCKER_LOGIN_SERVER is specified)
DOCKER_LOGIN_PASSWORD="${DOCKER_LOGIN_PASSWORD:-${CI_BUILD_TOKEN}}"
# TO: a tag to apply to the resulting image. If specified, we'll try to push the image after the build.
TO="${TO:-${CI_REGISTRY_IMAGE}}"

# Other variables (optional but no default value):
# DOCKER_LOGIN_EMAIL: email to use with docker login (when DOCKER_LOGIN_SERVER is specified)
# DOCKER_FILE: Name of the Dockerfile (Default is 'PATH/Dockerfile')
# BUILD_ARG: docker build arguments in key=value format. If several arguments are needed, provide BUILD_ARG_1, BUILD_ARG_2 etc.
# NO_CACHE: if set to any value, disable use of cache
# DOCKER_INFO: if set to any value, will print output of docker info before doing anything
# FROM: a tag to be used to override the source Dockerfile's FROM line

# we expect the host docker socket to be shared as /var/run/docker.sock; use that unless
# a specific docker daemon is specified in DOCKER_HOST (typically via a Docker-in-Docker service container)
# For some reason DOCKER_HOST (looks like tcp://docker:2375) is not always set when using a linked DinD service (possibly because both linked
# containers use the base name 'docker'?) but we can get the IP and port from DOCKER_PORT (looks like tcp://172.30.x.y:2375)
export DOCKER_HOST="${DOCKER_HOST:-${DOCKER_PORT:-unix:///var/run/docker.sock}}"

# workaround for https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/1380
if [ -e /.runonce ]; then
  echo "[gitlabci-docker-builder] Build already done (workaround for https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/1380)"
  exit 0
fi
touch /.runonce

# exit if any command returns error
set -e

# navigate to directory where code is checked out
cd "${CI_PROJECT_DIR}"

# docker info
if [ -n "${DOCKER_INFO}" ]; then
  echo [gitlabci-docker-builder] Running: docker info
  docker info
fi

# docker login
if [ -n "${DOCKER_LOGIN_SERVER}" ]; then
  echo "[gitlabci-docker-builder] Running: docker login ${DOCKER_LOGIN_SERVER}"
  docker login -u "${DOCKER_LOGIN_USERNAME}" -p "${DOCKER_LOGIN_PASSWORD}" -e "${DOCKER_LOGIN_EMAIL}" "${DOCKER_LOGIN_SERVER}"
fi

if [ -n "${FROM}" ]; then
  # replace only the first occurence of the FROM line (in case some text later in the Dockerfile matches as well)
  # http://www.linuxtopia.org/online_books/linux_tool_guides/the_sed_faq/sedfaq4_004.html
  echo "[gitlabci-docker-builder] Running: Replacing FROM for ${DOCKER_FILE:-${CONTEXT_DIR}/Dockerfile} with ${FROM}"
  sed -e "/^FROM .*$/{s||FROM ${FROM}|;:a" -e '$!N;$!ba' -e '}' -i "${DOCKER_FILE:-${CONTEXT_DIR}/Dockerfile}"
fi

# docker build
# CAUTION!! Due to variable number of arguments we use eval with user-provided variables, so we must
# be very careful with potential injection of code or undesired arguments.
# Properly quote and escape user-provided variables while preparing the command.
# We single-quote everything and escape single quotes in variables with sequence '\''
local build_cmd='docker build'
local escaped_single_quote="'\\''"

# always pull source image
build_cmd="${build_cmd} --pull"
if [ -n "${NO_CACHE}" -a "${NO_CACHE}" != "false" -a "${NO_CACHE}" != "0" ]; then
  build_cmd="${build_cmd} --no-cache"
fi
if [ -n "${DOCKER_FILE}" ]; then
  build_cmd="${build_cmd} --file '${DOCKER_FILE//'/$escaped_single_quote}'"
fi
if [ -n "${TO}" ]; then
  build_cmd="${build_cmd} --tag '${TO//'/$escaped_single_quote}'"
fi
# use any environment variable starting with BUILD_ARG to build the list of --build_arg options
# (equivalent of ${!BUILD_ARG*} in bash)
# || true so we don't stop while in set -e if grep finds no match
argvarnames=$(env | awk -F "=" '{print $1}' | grep -e "^BUILD_ARG" || true)
for argvarname in $argvarnames; do
  # equivalent to bashism: local buildarg="${!argvarname}"
  eval "local buildarg=\"\${${argvarname}}\""
  if [ -n "$buildarg" ]; then
    build_cmd="${build_cmd} --build-arg '${buildarg//'/$escaped_single_quote}'"
  fi
done
# provide the path to build
build_cmd="${build_cmd} '${CONTEXT_DIR//'/$escaped_single_quote}'"

echo "[gitlabci-docker-builder] Running: ${build_cmd}"
# the quotes are critical here to preserve the exact build command
eval "${build_cmd}"

if [ -n "${TO}" ]; then
  echo "[gitlabci-docker-builder] Running: docker push ${TO}"
  docker push "${TO}"
fi
