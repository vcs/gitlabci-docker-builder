# Overview

A Docker image that can be used to build Docker images in GitLab-CI.

# Why this is needed

Building Docker images in GitLab CI [requires special configuration](http://docs.gitlab.com/ce/ci/docker/using_docker_build.html#runner-configuration),
either having access to the host's Docker daemon or having a privileged Docker-in-Docker service container
providing a containerized Docker daemon. Because either option enables escaping the container, user-provided
commands must not be allowed with such configuration in a shared runner environment in order
to preserve job isolation.

This image will allow building a Docker image from a GitLab repository and pushing it to a registry without allowing
user-provided commands. When run with a Docker-in-Docker (DinD) service container, it will use the DinD daemon;
otherwise it will try to use the host's Docker daemon.

# Controlling the behavior of the build

The following environment variables allow to control the behaviour of the Docker image build.
They are all optional:

`FROM`: a tag to be used to override the source Dockerfile's FROM line. This allows to build a same Dockerfile for multiple base images.

`TO`: a tag to apply to the resulting image. If specified, we'll try to push the image after the build. By default: `$CI_REGISTRY_IMAGE`. Check the information about [build variables](https://gitlab.cern.ch/help/ci/variables/README.md) for more details.

`CONTEXT_DIR`: folder where the Dockerfile to build is located; we'll run the docker build command in that folder. This enables building different Dockerfiles from a single repository (by default: repository root)

`DOCKER_FILE`: name of the Dockerfile (by default: 'Dockerfile')

`DOCKER_LOGIN_SERVER`: a docker registry to login into (allows to provide credentials for pushing the image). By default: gitlab-registry.cern.ch (`$CI_REGISTRY`). Check the information about [build variables](https://gitlab.cern.ch/help/ci/variables/README.md) for more details).

`DOCKER_LOGIN_USERNAME`: username for login into the docker registry (allows to provide credentials for pushing the image). By default: `gitlab-ci-token`.

`DOCKER_LOGIN_PASSWORD`: password for login into the docker registry (allows to provide credentials for pushing the image). By default `$CI_BUILD_TOKEN`.  Check the information about [build variables](https://gitlab.cern.ch/help/ci/variables/README.md) for more details.

`DOCKER_LOGIN_EMAIL`: email to use with docker login

`BUILD_ARG`: docker build arguments in key=value format. If several arguments are needed, provide `BUILD_ARG_1`, `BUILD_ARG_2` etc.

`NO_CACHE`: if set to any value, disable use of [the build cache](https://docs.docker.com/engine/userguide/eng-image/dockerfile_best-practices/#build-cache) (default: cache is enabled)

`DOCKER_INFO`: if set to any value, will print output of docker info before doing anything (debug purposes)

